
function [] = final_solution()
    global kValues;
    global numberOfPartsToDivideData;
    global dataCols;
    global newDataCols;
    global catagoryCol;
    global newCatagoryCol;

    kValues = 1:25;
    numberOfPartsToDivideData = 10;

    dataCols = 2:10;                     % 1 column is meaningless label
    newDataCols = dataCols - 1;         % In the code we use this because we remove the first column
    catagoryCol = 11;                    % 11 column is the catagory (which shouldn't count as data)
    newCatagoryCol = catagoryCol - 1;   % In the code we use this because we remove the first column

    load glass.data;
    dataGlass = glass(:,  [dataCols, catagoryCol]);

    [noStdLossValues, noStdLossRatios, stdLossValues, stdLossRatios] = getLostForTable(dataGlass);

    plotResults(noStdLossRatios, stdLossRatios);
    %plotResults(noStdLossValues, stdLossValues);
end

function [noStdGlass, stdGlass] = getGlassTables(dataGlass)
    global catagoryCol;
    
    noStdGlass = [dataGlass, glass(:, catagoryCol)];
    stdGlass = [zscore(dataGlass), glass(:, catagoryCol)];
end

function [trainingSamples, testSamples] = divideDataToSamplesTypes(table, partsIndexes, allIndexes, numberOfRows)
    global numberOfPartsToDivideData;

    %  Random which one of the 10 parts will be used for test samples
    % (The other 9 will be used for training)
    randIndex = randi(numberOfPartsToDivideData);
    testSamplesPartStartIndex = partsIndexes(randIndex);
    if (randIndex == numberOfPartsToDivideData)
        testSamplesPartEndIndex = numberOfRows;
    else
        testSamplesPartEndIndex = partsIndexes(1,randIndex+1) -1;
    end   
    testSamples = table(testSamplesPartStartIndex:testSamplesPartEndIndex, :);
    trainingSampleIndexes = [allIndexes(1:testSamplesPartStartIndex-1), allIndexes(testSamplesPartEndIndex+1:numberOfRows)];
    trainingSamples = table(trainingSampleIndexes, :);
end

function [smallestNIdx] = getkMinIndexes(A, n)
    [ASorted AIdx] = sort(A);
    smallestNIdx = AIdx(1:n);
end

function [testSampleCatagory] = getTestSampleCatagory(trainingSamples, testSample, k)
    global newDataCols;
    global newCatagoryCol;

    numberOfTrainingSamples = size(trainingSamples, 1);
    distancesVector = trainingSamples - repmat(testSample, numberOfTrainingSamples, 1);
    distancesFromEachTrainingPoint = sum(distancesVector(:,newDataCols).^2, 2);
    kMinIndexes = getkMinIndexes(distancesFromEachTrainingPoint, k);

    minTrainingSamplesCatagories = trainingSamples(kMinIndexes, newCatagoryCol);
    testSampleCatagory = mode(minTrainingSamplesCatagories);
end

function [stdMatrix] = standardizeMatrix(matrix, std, mean)
    global newDataCols;
    global newCatagoryCol;

    % Temporary remove catagory column from the calculations
    catagoryColumn = matrix(:, newCatagoryCol);
    stdMatrix = matrix(:, newDataCols);
    
    numberOfRowsInMatrix = size(stdMatrix, 1);
    stdMatrix = stdMatrix - repmat(mean, numberOfRowsInMatrix, 1);
    stdMatrix = bsxfun(@rdivide, stdMatrix, repmat(std, numberOfRowsInMatrix, 1));
    
    % Restore the catagory column
    stdMatrix = [stdMatrix, catagoryColumn];
end

function [lossValue] = getLossValueBykNN(trainingSamples, testSamples, k)
    global newCatagoryCol;
    
    % For each sample on the sample test, calculate it's catagory based on k-NN
    % and compare the result agains it's real catagory on the given data
    % If the prediction was mistaken, count it as a loss
    lossValue = 0;
    numberOfTestSamples = size(testSamples, 1);
    for testSampleIndex = 1:numberOfTestSamples
        testSample = testSamples(testSampleIndex,:);
        [testSampleCatagory] = getTestSampleCatagory(trainingSamples, testSample, k);
        realTestSampleCatagory = testSample(newCatagoryCol);
        if testSampleCatagory ~= realTestSampleCatagory
            lossValue = lossValue + 1;
        end
    end
end

function [lossValues, lossRatios, stdLossValues, stdLossRatios] = getLostForTable(dataGlass)
    global kValues;
    global numberOfPartsToDivideData;
    global newDataCols;

    % Divide that data into equal parts
    numberOfRows = size(dataGlass, 1);
    allIndexes = 1:numberOfRows;
    partsIndexes = 1:ceil(numberOfRows/numberOfPartsToDivideData):numberOfRows;

    lossValues = zeros(1, size(kValues, 2));
    lossRatios = zeros(1, size(kValues, 2));
    
    stdLossValues = zeros(1, size(kValues, 2));
    stdLossRatios = zeros(1, size(kValues, 2));
    
    for k = kValues
        currentLossValue = 0;
        currentStdLossValue = 0;
        for i = 1:numberOfPartsToDivideData
            % Shuffle table rows
            randomRowsPermutation = randperm(numberOfRows);
            dataGlass = dataGlass(randomRowsPermutation, :);
            
            [trainingSamples, testSamples] = divideDataToSamplesTypes(dataGlass, partsIndexes, allIndexes, numberOfRows);
            
            % Standardize samples
            trainingSamplesStd = std(trainingSamples(:, newDataCols));
            trainingSamplesMean = mean(trainingSamples(:, newDataCols));
            [stdTrainingSamples] = standardizeMatrix(trainingSamples, trainingSamplesStd, trainingSamplesMean);
            [stdTestSamples] = standardizeMatrix(testSamples, trainingSamplesStd, trainingSamplesMean);
            
            [currentIterationLossValue] = getLossValueBykNN(trainingSamples, testSamples, k);
            currentLossValue = currentLossValue + currentIterationLossValue;
            
            [currentIterationStdLossValue] = getLossValueBykNN(stdTrainingSamples, stdTestSamples, k);
            currentStdLossValue = currentStdLossValue + currentIterationStdLossValue;
        end
        lossValues(k) = currentLossValue;
        lossRatios(k) = currentLossValue / numberOfRows;
        
        stdLossValues(k) = currentStdLossValue;
        stdLossRatios(k) = currentStdLossValue / numberOfRows;
    end
end

function [] = plotResults(noStdLossResults, stdLossResults)
    global kValues;

    plot(kValues, noStdLossResults, kValues, stdLossResults);
    legend('Without Standirzation', 'With Standirzation');
end


